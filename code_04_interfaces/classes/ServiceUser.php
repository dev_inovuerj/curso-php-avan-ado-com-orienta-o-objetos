<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 12/11/16
 * Time: 15:40
 */

namespace Services;

class ServiceUser
{

    /**
     * @var PDO
     */
    private $db;

    function __construct(PDO $resource)
    {
        $this->db = $resource;
    }

    /**
     * @return PDO
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param PDO $db
     */
    public function setDb($db)
    {
        $this->db = $db;

        return $this;
    }


    /******* CRUD ********/

    public function retrive($order = '')
    {

        $ORDER_BY = !!$order ? " ORDER BY {$order}" : '';

        $stmt = $this->db->prepare("SELECT * FROM usuario {$ORDER_BY} ");

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function find($id)
    {
        $stmt = $this->db->prepare("SELECT name,email FROM usuario WHERE id=? ");
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        $stmt->execute();

        $stmt->bindColumn(1, $name);
        $stmt->bindColumn(2, $email);


        $stmt->fetch();

        return [
            'id' => $id,
            'name' => $name,
            'email' => $email
        ];


    }

    /**
     * @return int
     */
    public function insert()
    {
        $stmt = $this->db->prepare("INSERT INTO usuario(name,email) VALUES (?,?)");
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->email);
        $stmt->execute();

        $this->setId($this->db->lastInsertId());


        return $this;

    }

    public function update()
    {
        print "<pre>";
//        var_dump($stmt->debugDumpParams());
        print "</pre>";


        $stmt = $this->db->prepare("UPDATE usuario SET name=?, email=? WHERE id=?");
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->email);
        $stmt->bindParam(3, $this->id);

        $stmt->execute();

    }

    public function delete()
    {
        $stmt = $this->db->prepare("DELETE FROM usuario WHERE id=?");
        $stmt->bindParam(1, $this->id);

        $stmt->execute();
    }


}