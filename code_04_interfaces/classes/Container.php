<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 14/11/16
 * Time: 17:58
 *
 * Conceito de Service Container implentado.
 *
 */
class Container
{
    public static function getProduct()
    {
        return new Product(self::getConn());
    }

    public static function getConn()
    {
        return new Conn(DSN_MYSQL, 'root', '1234');
    }
}