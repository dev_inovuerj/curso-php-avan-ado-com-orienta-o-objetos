<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 11/11/16
 * Time: 13:57
 */

function __autoload($class)
{

    $base_path = $_SERVER['DOCUMENT_ROOT'] . '/';
    $parts = explode('\\', $class);
    $file_class_php = end($parts) . '.php';
    $path = "";


    if (file_exists($base_path . $file_class_php)) {

        $path = $base_path . $file_class_php;

    } else if (file_exists($base_path . 'classes/' . $file_class_php)) {

        $path = "{$base_path}classes/{$file_class_php}";

    }

    (strlen($path) > 0) ? require_once $path : false;
}


///* Banco de dados */
//define('DSN_MYSQL', 'mysql:host=localhost;dbname=test');
//
//$pdo_mysql = new PDO(DSN_MYSQL, 'root', '1234',
//
//    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
//
//);


