<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 11/11/16
 * Time: 18:08
 */

require_once "../configurations.php";
require_once "../vendor/autoload.php";

/*Pimple controla o uso de dependencias */
use Pimple\Container;

/*Gerenciador de Serviços/Dependências */
$container = new Container();

/*A diferença entre dar um new \DateTIme() é, que o __AUTOLOAD será feito*/
/* Serviçoes são instanciados um unica vez*/

$container['conn'] = function () {

    /*  Retorna o mesmo objeto por padrao*/
    return new \Source\Conn(DSN_MYSQL, 'root', '1234');

};


/**
 * Pega a ultima dependencias em fluxo de programa (conn) e passa para $c
 * @param $c
 * @return \Source\Product
 */
$container['product'] = function ($c) {

    return new \Source\Product($c['conn']);

};


//debug('Container CONN ', $container['conn']);

debug('Container PRODUCTS ', $container['product']->retrieve());

//define('DSN_MYSQL', "mysql:host=localhost;dbname=test");
//
//$conn_mysql = new Conn(DSN_MYSQL, 'root', '1234');
//
//$user_product = new Product($conn_mysql);
//
//
//print "<pre>";
//var_dump($user_product->retrive());
//
