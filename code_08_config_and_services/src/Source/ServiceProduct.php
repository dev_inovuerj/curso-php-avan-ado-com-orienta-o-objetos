<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 13:59
 */

namespace Source;


class ServiceProduct implements IServiceProduct
{
    protected $_db;

    public function __construct(IConn $db, IProduct $product)
    {
        $this->_db = $db->connect();
    }

    /* Assinatura com um tipo IConn, que é um PDO*/
    public function retrieve()
    {
        // TODO: Implement retrive() method.

        $query = "SELECT * FROM products";

        $stmt = $this->_db->prepare($query);

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }

    public function save()
    {
        // TODO: Implement save() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }


}