<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 14:32
 */

namespace Source;


interface IProduct
{
    public function getId();

    public function setId($id);

    public function getName();

    public function setName($name);

    public function getDesc();

    public function setDesc($desc);

}