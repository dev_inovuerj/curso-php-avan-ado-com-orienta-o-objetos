<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 13:48
 */

namespace Source;


interface IServiceProduct
{

//    /* Assinatura com um tipo IConn, que é um PDO*/
    public function __construct(IConn $db, IProduct $product);

    public function retrieve();

    public function save();

    public function update();

    public function delete();

}