<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 13:19
 */


/*A diferença entre dar um new \DateTIme() é, que o __AUTOLOAD será feito*/
/* Serviçoes são instanciados um unica vez*/

$container['conn'] = function ($container_default) {

    /*  Retorna o mesmo objeto por padrao*/
    return new \Source\Conn($container_default['dsn'], $container_default['user'], $container_default['pass']);

};


/**
 * Pega a ultima dependencias em fluxo de programa (conn) e passa para $c
 * @param $c
 * @return \Source\Product
 */
$container['product'] = function () {

    return new \Source\Product();

};


$container['ServiceProduct'] = function ($c) {

    /*Para construir um Serviço para produto é necessário passar conexao e um produto*/
    return new \Source\ServiceProduct($c['conn'], $c['product']);

};