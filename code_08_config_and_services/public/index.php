<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 11/11/16
 * Time: 18:08
 */

require_once "../vendor/autoload.php";

require_once "bootstrap.php";

require_once "config.php";

require_once "service.php";

require_once 'product.list.php';

debug('Container PRODUCTS List ', $container['ServiceProduct']->retrieve());

$products = $container['ServiceProduct']->retrieve();

tableListProducts($products);


/**
 *  - Considerações finais e Conceitos aprendidos
 *
 *      Abstração,
 *      Serviços,
 *      Injeção de Dependencias,
 *      PDO,
 */


