<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 15/11/16
 * Time: 14:40
 */

function tableListProducts($products)
{

    ob_start();
    ?>


    <style>


        #tbl_products {
            border-collapse: collapse;
            width: 100%;
            border: 2px double darkblue;
            padding: 10px;
            margin-top: 10px;

        }

        #tbl_products td, #tbl_products th {
            /*border: 1px solid black;*/

            padding: 15px;
            text-align: left;
            border-bottom: 1px solid #ddd;

        }

        #tbl_products td {

            height: 50px;
            vertical-align: bottom;

        }

        #tbl_products th {
            height: 50px;
            text-align: left;
        }

        #tbl_products tr:hover {
            background-color: #f5f5f5
        }

        #tbl_products tr:nth-child(even) {
            background-color: #f2f2f2
        }

        #tbl_products th {
            background-color: #4CAF50;
            color: white;
        }

        #tbl_products caption {
            caption-side: bottom;
        }

    </style>

    <div style="overflow-x:auto;">


        <table id="tbl_products">

            <caption>Products</caption>

            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Desc</th>
            </tr>

            <?PHP foreach ($products as $product) : ?>

                <tr>
                    <td><?PHP echo $product['id']; ?></td>
                    <td><?PHP echo $product['name']; ?></td>
                    <td><?PHP echo $product['desc']; ?></td>
                </tr>

            <?PHP endforeach; ?>

        </table>

    </div>

    <?PHP

    $table = ob_get_contents();
    ob_end_clean();


    echo $table;


}

?>