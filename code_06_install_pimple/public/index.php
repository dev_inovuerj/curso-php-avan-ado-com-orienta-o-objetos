<?php
/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 11/11/16
 * Time: 18:08
 */


require_once "../bootstrap.php";
require_once "../vendor/autoload.php";
error_reporting(true);

/*Pimple controla o uso de dependencias */
use Pimple\Container;

/*Gerenciador de Serviços/Dependências */
$container = new Container();

/*Criando serviço de conexão - Class Conn */
$container['conn'] = function () {

    return new Source\Conn(DNS, 'root', '1234');

};


//define('DSN_MYSQL', "mysql:host=localhost;dbname=test");
//
//$conn_mysql = new Conn(DSN_MYSQL, 'root', '1234');
//
//$user_product = new Product($conn_mysql);
//
//
//print "<pre>";
//var_dump($user_product->retrive());
//
