<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 14/11/16
 * Time: 17:21
 */

namespace Source;

class Product
{
    private $db;

    public function __construct(IConn $db)
    {
        $this->db = $db->connect();

    }

    public function retrive()
    {
        $query = "SELECT * FROM products";

        $stmt = $this->db->prepare($query);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }
}