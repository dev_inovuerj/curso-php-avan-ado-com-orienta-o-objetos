<?php

/**
 * Created by PhpStorm.
 * User: emilio
 * Date: 14/11/16
 * Time: 17:39
 */


class Conn implements IConn
{

    private $dns,
        $user,
        $pass;

    /**
     * Conn constructor.
     * @param $dns
     * @param $user
     * @param $pass
     */
    public function __construct($dns, $user, $pass)
    {
        $this->dns = $dns;
        $this->user = $user;
        $this->pass = $pass;
    }


    public function connect()
    {
        // TODO: Implement connect() method.

        return new PDO($this->dns, $this->user, $this->pass,

            /* Working UTF-8 MySQL Built connection */
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")

        );


    }
}